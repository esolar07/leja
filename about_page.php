<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="cupcakes" content="">
        <title>LEJA CUPCAKES</title>
        <link rel="stylesheet" type="text/css" href="LEJA_CSS.css"/>
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine"/>
	</head>
	
    <body>
		
		<h1>Leja Cupcakes</h1>
       
       <?php include "menu.php"; ?>
        
        <div id="two">
        	<h2>About Leja</h2>
        	<p>Cupcakes are known today as teeny, delectable desserts. But did you know that cupcakes have been around since the 19th century? Then, they were made with simple cake recipes, and they were far from the decadent desserts we can find today. Although classic vanilla with chocolate icing options are still popular and evoke a sense of nostalgia, upscale bakeries and even people who bake for a hobby have created elaborate flavors with even more elaborate options for design.</p><br>
        	<img id="kids" src="https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-frc3/300910_2254345073609_310875378_n.jpg" height="350"width="350"/>
        	<p>This website is designed to let you know that there's a little more to the cupcake than meets the eye. Explore the history of the cupcake, as well as the cupcake's increasingly prevalent role as a trendy item. Learn tips and tricks to creating your own cupcakes, and try out some recipes. Under the "Cupcakes Today" link, I have included a slide show of some examples of how people decorate cupcakes - they're more complex than you might think!</p><br>
        	<p>What I hope you get from this site is a greater understanding and hopefully a greater appreciation for these desserts. Cupcakes can remind us of elementary school days or they can become decadent works of art.</p>
		</div>
		<footer id="footer">
  			<p>What ever rights reseved... Site crafted by Eddie Solar </p>
  		</footer> 
	</body>
</html>

